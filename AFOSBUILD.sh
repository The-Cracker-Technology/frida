rm -rf /opt/ANDRAX/frida

source /opt/ANDRAX/PYENV/python3-1/bin/activate

/opt/ANDRAX/PYENV/python3-1/bin/pip3 install frida-tools==12.4.2 frida==16.2.5

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Pip install frida-tools and frida bindings... PASS!"
else
  # houston we have a problem
  exit 1
fi


cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
